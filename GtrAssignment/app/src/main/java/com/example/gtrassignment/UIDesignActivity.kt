package com.example.gtrassignment

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.NavigationUI.setupWithNavController
import com.example.gtrassignment.databinding.ActivityInfoBinding
import com.example.gtrassignment.databinding.ActivityUidesignBinding

class UIDesignActivity : AppCompatActivity() {

    private lateinit var binding: ActivityUidesignBinding

    private var appBarConfiguration: AppBarConfiguration? = null
    private var navController: NavController? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityUidesignBinding.inflate(layoutInflater)
        setContentView(binding.root)

        appBarConfiguration = AppBarConfiguration.Builder(
            R.id.navigation_home,
            R.id.navigation_cart,
            R.id.navigation_favorite,
            R.id.navigation_profile
        ).build()

        /** Initialize NavController.  */
        navController = Navigation.findNavController(this, R.id.navHostFragment)
        setupWithNavController(binding.bottomNavView, navController!!)
    }
}