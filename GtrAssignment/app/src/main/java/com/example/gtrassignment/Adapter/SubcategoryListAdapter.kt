package com.example.gtrassignment.Adapter

import android.content.Context
import android.graphics.Paint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.example.gtrassignment.Model.CategoryModel
import com.example.gtrassignment.Model.SubcategoryModel
import com.example.gtrassignment.R
import com.example.gtrassignment.databinding.LayoutCatogoryItemBinding
import com.example.gtrassignment.databinding.LayoutSubcategoryItemBinding

class SubcategoryListAdapter(
    private val context: Context,
    private val subcategoryResponse: List<SubcategoryModel>
) : RecyclerView.Adapter<SubcategoryListAdapter.MyViewHolder>() {
    class MyViewHolder(val itemBinding: LayoutSubcategoryItemBinding): RecyclerView.ViewHolder(itemBinding.root) {
        fun bind(dataBean: SubcategoryModel) {
            itemBinding.txtProductName.text = dataBean.productName
            itemBinding.txtOldPrice.text = dataBean.productOldPrice
            itemBinding.txtNewPrice.text = dataBean.productNewPrice
            itemBinding.imgProductsImage.setImageResource(dataBean.productImage)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(
            LayoutSubcategoryItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun getItemCount(): Int {
        return subcategoryResponse.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(subcategoryResponse[position])

        holder.itemBinding.txtOldPrice.paintFlags = holder.itemBinding.txtOldPrice.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
        holder.itemBinding.txtOldPrice.setBackgroundResource(R.drawable.text_background)

        holder.itemBinding.txtAddToCart.setOnClickListener {
            holder.itemBinding.txtAddToCart.visibility = View.GONE
            holder.itemBinding.layoutItemCart.visibility = View.VISIBLE
        }

        holder.itemBinding.imgFavorite.setOnClickListener {
            holder.itemBinding.imgFavorite.setImageResource(R.drawable.heart)
        }

        var itemCount: Int = 1
        var totalCount: Int = 0
        holder.itemBinding.layoutPlus.setOnClickListener {
            totalCount = itemCount++
            holder.itemBinding.txtCountCart.text = totalCount.toString()
        }

        holder.itemBinding.layoutMinus.setOnClickListener {
            totalCount = --itemCount
            holder.itemBinding.txtCountCart.text = totalCount.toString()
        }

    }
}