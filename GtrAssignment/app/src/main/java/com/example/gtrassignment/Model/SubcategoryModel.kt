package com.example.gtrassignment.Model

data class SubcategoryModel(
    val productName: String,
    val productImage: Int,
    val productOldPrice: String,
    val productNewPrice: String
)
