package com.example.gtrassignment.Fragments.Home
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.denzcoskun.imageslider.constants.ScaleTypes
import com.denzcoskun.imageslider.models.SlideModel
import com.example.gtrassignment.Adapter.CategoryListAdapter
import com.example.gtrassignment.Model.CategoryModel
import com.example.gtrassignment.R
import com.example.gtrassignment.databinding.FragmentHomeBinding
import java.util.*


class HomeFragment : Fragment() {

    private lateinit var binding: FragmentHomeBinding
    lateinit var categoryList: List<CategoryModel>
    var adapter: CategoryListAdapter? = null
    var layoutManager: LinearLayoutManager? = null

    companion object {
        fun newInstance() = HomeFragment()
    }

    private lateinit var viewModel: HomeViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentHomeBinding.inflate(inflater, container, false)

        val imageList = ArrayList<SlideModel>()

        imageList.add(SlideModel(R.drawable.slider_image, ""))
        imageList.add(SlideModel(R.drawable.slider_image, ""))
        imageList.add(SlideModel(R.drawable.slider_image, ""))
        imageList.add(SlideModel(R.drawable.slider_image, ""))

        binding.imageSlider.setImageList(imageList, ScaleTypes.FIT)

        categoryList = ArrayList<CategoryModel>()

        // on below line we are adding data to
        // our course list with image, product name, old price, new price.
        categoryList = categoryList + CategoryModel(R.drawable.catogory_frouits, "Fruits and vegetables", "This food is good for health")
        categoryList = categoryList + CategoryModel(R.drawable.grocery, "Grocery and Staples", "This food is good for health")
        categoryList = categoryList + CategoryModel(R.drawable.household, "Household Needs", "This food is good for health")
        categoryList = categoryList + CategoryModel(R.drawable.man, "Mans and Womens Wear", "This were is good for health")
        categoryList = categoryList + CategoryModel(R.drawable.foot, "Foot ware", "This foot ware is good for health")
        categoryList = categoryList + CategoryModel(R.drawable.catogory_frouits, "Fruits and vegetables", "This food is good for health")
        categoryList = categoryList + CategoryModel(R.drawable.grocery, "Grocery and Staples", "This food is good for health")
        categoryList = categoryList + CategoryModel(R.drawable.household, "Household Needs", "This food is good for health")
        categoryList = categoryList + CategoryModel(R.drawable.man, "Mans and Womens Wear", "This were is good for health")
        categoryList = categoryList + CategoryModel(R.drawable.foot, "Foot ware", "This foot ware is good for health")

        // on below line we are initializing our course adapter
        // and passing product list and context.

        layoutManager = LinearLayoutManager(requireContext())
        binding.recyclerCategory.layoutManager = layoutManager
        adapter = CategoryListAdapter(requireContext(), this, categoryList)
        binding.recyclerCategory.setHasFixedSize(true)
        binding.recyclerCategory.adapter = adapter

        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(HomeViewModel::class.java)
        // TODO: Use the ViewModel
    }

}