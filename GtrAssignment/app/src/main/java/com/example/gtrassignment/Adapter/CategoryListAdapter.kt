package com.example.gtrassignment.Adapter

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.RecyclerView
import com.example.gtrassignment.Fragments.Subcategory.SubcategoryFragment
import com.example.gtrassignment.Fragments.Home.HomeFragment
import com.example.gtrassignment.Model.CategoryModel
import com.example.gtrassignment.R
import com.example.gtrassignment.databinding.LayoutCatogoryItemBinding

class CategoryListAdapter(
    private val context: Context,
    var homeFragment: HomeFragment,
    private val categoryResponse: List<CategoryModel>
) :
    RecyclerView.Adapter<CategoryListAdapter.MyViewHolder>() {

    class MyViewHolder(val itemBinding: LayoutCatogoryItemBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {
        fun bind(dataBean: CategoryModel) {
            itemBinding.txtCategoryName.text = dataBean.categoryName
            itemBinding.txtCategoryDescription.text = dataBean.categoryDescription
            itemBinding.imgCategory.setImageResource(dataBean.categoryImage)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(
            LayoutCatogoryItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun getItemCount(): Int {
        return categoryResponse.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(categoryResponse[position])

        holder.itemView.setOnClickListener {

            val fruitsFragment = SubcategoryFragment()
            val fm: FragmentManager = homeFragment.parentFragmentManager
            val ft = fm.beginTransaction()
            val bundle = Bundle()
            bundle.putString("CATEGORY_NAME", categoryResponse!![position].categoryName!!)

            fruitsFragment.arguments = bundle

            Log.e("TAG: ", "" + categoryResponse!![position].categoryName!!)

            ft.replace(R.id.navHostFragment, fruitsFragment)
            ft.addToBackStack(null)
            ft.commit()

        }
    }
}