package com.example.gtrassignment

import android.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.gtrassignment.Adapter.DataListAdapter
import com.example.gtrassignment.Model.GetDataResponse
import com.example.gtrassignment.ViewModel.MainActivityViewModel
import com.example.gtrassignment.databinding.ActivityMainBinding
import dmax.dialog.SpotsDialog

class MainActivity : AppCompatActivity() {

    var mainActivityViewModel: MainActivityViewModel? = null
    private lateinit var binding: ActivityMainBinding
    val token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJVc2VySWQiOiI3OCIsIkN1cnJlbnRDb21JZCI6IjEiLCJuYmYiOjE2ODE3MDI5OTAsImV4cCI6MTY4MjMwNzc5MCwiaWF0IjoxNjgxNzAyOTkwfQ.JCU1MPH_SOJsHYpOn9GKrYx90N3Tsdtut3rTU3Hl09g"

    var adapter: DataListAdapter? = null
    var layoutManager: LinearLayoutManager? = null

    var dialog: AlertDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        mainActivityViewModel = ViewModelProvider(this)[MainActivityViewModel::class.java]

        dialog = SpotsDialog.Builder().setContext(this).setCancelable(false).build()
        dialog!!.show()

        /*mainActivityViewModel!!.getDataList.observe(this, { dataModels ->
            Log.e("MainActivity", "DataList: " + dataModels.get(0).Product().brandName)

            adapter=  PostListAdapter(this, postModels)
            adapter!!.notifyDataSetChanged()
            recyclerPost!!.adapter = adapter
                    dialog!!.dismiss()
        })*/


        mainActivityViewModel!!.getDataList(token)
            .observe(this) { getDataResponse ->
                if (getDataResponse != null) {
                    Log.e("MAIN ACTIVITY", "Data: " + getDataResponse.productList)
                    getDataResponse.productList?.let { setDataInAdapter(it) }
                    dialog!!.dismiss()

                } else {
                    dialog!!.dismiss()
                }
            }
    }

    private fun setDataInAdapter(productList: List<GetDataResponse.Product>) {
        layoutManager = LinearLayoutManager(this)
        binding.recyclerData.layoutManager = layoutManager
        adapter = DataListAdapter(this, productList)
        binding.recyclerData.setHasFixedSize(true)
        binding.recyclerData.adapter = adapter
        dialog!!.dismiss()
    }
}