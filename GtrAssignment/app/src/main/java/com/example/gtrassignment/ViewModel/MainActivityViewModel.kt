package com.example.gtrassignment.ViewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.example.gtrassignment.MainActivity
import com.example.gtrassignment.Model.GetDataResponse
import com.example.gtrassignment.Repositories.MainActivityRepo

class MainActivityViewModel : ViewModel() {
//    private val mainRepo: MainActivityRepo
//
//    init {
//        mainRepo = MainActivityRepo()
//    }
//
//    val getDataList: LiveData<MutableList<GetDataResponse>>
//        get() = mainRepo.getDataModelLiveData

    private val repo: MainActivityRepo = MainActivityRepo()
    fun getDataList(auth_token: String): LiveData<GetDataResponse> {
        return repo.getDataModelLiveData(auth_token)
    }
}