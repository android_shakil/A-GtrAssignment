package com.example.gtrassignment.Model

data class CategoryModel(
    val categoryImage: Int,
    val categoryName: String,
    val categoryDescription: String
)