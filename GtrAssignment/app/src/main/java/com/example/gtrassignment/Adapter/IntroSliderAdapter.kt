package com.example.gtrassignment.Adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.AppCompatImageView
import androidx.recyclerview.widget.RecyclerView
import com.example.gtrassignment.Model.CategoryModel
import com.example.gtrassignment.R
import com.example.gtrassignment.Slider.IntroSlider
import com.example.gtrassignment.databinding.LayoutCatogoryItemBinding
import com.example.gtrassignment.databinding.LayoutDataItemBinding
import com.example.gtrassignment.databinding.SliderItemContainerBinding

class IntroSliderAdapter(private val introSliders: List<IntroSlider>): RecyclerView.Adapter<IntroSliderAdapter.IntroSliderViewHolder>() {
    class IntroSliderViewHolder(val itemBinding: SliderItemContainerBinding): RecyclerView.ViewHolder(itemBinding.root){
        fun bind(dataBean: IntroSlider) {
            itemBinding.imgTitle.setImageResource(dataBean.image)
            itemBinding.imageSlider.setImageResource(dataBean.icon)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): IntroSliderViewHolder {
        return IntroSliderViewHolder(
            SliderItemContainerBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun getItemCount(): Int {
        return introSliders.size
    }

    override fun onBindViewHolder(holder: IntroSliderViewHolder, position: Int) {
        holder.bind(introSliders[position])
    }
}