package com.example.gtrassignment.Adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.gtrassignment.Model.GetDataResponse
import com.example.gtrassignment.databinding.LayoutDataItemBinding

class DataListAdapter(
    private val context: Context,
    private val dataResponse: List<GetDataResponse.Product>
) : RecyclerView.Adapter<DataListAdapter.MyViewHolder>() {
    class MyViewHolder(val itemBinding: LayoutDataItemBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {
        fun bind(dataBean: GetDataResponse.Product) {
            itemBinding.txtBrandName.text = StringBuilder("Brand Name: ").append(dataBean.brandName)
            itemBinding.txtName.text = StringBuilder("Name: ").append(dataBean.name)
            itemBinding.txtPrice.text = StringBuilder("Price: ").append(dataBean.price)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(
            LayoutDataItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun getItemCount(): Int {
        return dataResponse.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(dataResponse[position])
    }
}