package com.example.gtrassignment.Network

import com.example.gtrassignment.Model.GetDataResponse
import io.reactivex.Observable
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Header

interface ApiService {
    @GET("invoiceapps/Values/GetProductList/")
    fun getData(
        @Header("Authorization") auth_token: String
    ): Call<GetDataResponse>
}