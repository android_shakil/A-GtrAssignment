package com.example.gtrassignment.Fragments.Subcategory

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.gtrassignment.Adapter.SubcategoryListAdapter
import com.example.gtrassignment.Fragments.Home.HomeFragment
import com.example.gtrassignment.Model.SubcategoryModel
import com.example.gtrassignment.R
import com.example.gtrassignment.databinding.FragmentSubcategoryBinding

class SubcategoryFragment : Fragment() {

    private lateinit var binding: FragmentSubcategoryBinding
    // on below line we are creating
    // variables for product list
    lateinit var productList: List<SubcategoryModel>
    private var categoryName: String? = null
    var adapter: SubcategoryListAdapter? = null
    var layoutManager: LinearLayoutManager? = null

    companion object {
        fun newInstance() = SubcategoryFragment()
    }

    private lateinit var viewModel: SubcategoryViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentSubcategoryBinding.inflate(inflater, container, false)

        val bundle = this.arguments
        if (bundle != null) {
            categoryName = bundle.getString("CATEGORY_NAME", null)
            binding.txtCategory.text = categoryName
            Log.e("Fruits", "CATEGORY_NAME: " + categoryName)
        }

        binding.backArrow.setOnClickListener {
            val homeFragment = HomeFragment()
            val fm = fragmentManager
            val ft = fm!!.beginTransaction()

            ft.replace(R.id.navHostFragment, homeFragment)
            ft.addToBackStack(null)
            ft.commit()
        }

        productList = ArrayList<SubcategoryModel>()

        /** on below line we are adding data to
         * our course list with image, product name, old price, new price. **/
        productList = productList + SubcategoryModel("Broccoli flower", R.drawable.broccoli, "৳ 200", "৳ 160")
        productList = productList + SubcategoryModel("Pomegranate", R.drawable.pomegranate, "৳ 150", "৳ 100")
        productList = productList + SubcategoryModel("Green Apple", R.drawable.apple, "৳ 130", "৳ 120")
        productList = productList + SubcategoryModel("Green Capsicum", R.drawable.green_capsicum, "৳ 300", "৳ 170")
        productList = productList + SubcategoryModel("Red Capsicum", R.drawable.red_capsicum, "৳ 180", "৳ 150")
        productList = productList + SubcategoryModel("Capsicum", R.drawable.capsicum, "৳ 160", "৳ 140")
        productList = productList + SubcategoryModel("Broccoli flower", R.drawable.broccoli, "৳ 200", "৳ 160")
        productList = productList + SubcategoryModel("Pomegranate", R.drawable.pomegranate, "৳ 150", "৳ 100")
        productList = productList + SubcategoryModel("Green Apple", R.drawable.apple, "৳ 130", "৳ 120")
        productList = productList + SubcategoryModel("Green Capsicum", R.drawable.green_capsicum, "৳ 300", "৳ 170")
        productList = productList + SubcategoryModel("Red Capsicum", R.drawable.red_capsicum, "৳ 180", "৳ 150")
        productList = productList + SubcategoryModel("Capsicum", R.drawable.capsicum, "৳ 160", "৳ 140")


        /** on below line we are initializing our course adapter
         * and passing product list and context. **/

        layoutManager = GridLayoutManager(requireContext(), 2)
        binding.recyclerSubcategory.layoutManager = layoutManager
        adapter = SubcategoryListAdapter(requireContext(), productList)
        binding.recyclerSubcategory.setHasFixedSize(true)
        binding.recyclerSubcategory.adapter = adapter

        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(SubcategoryViewModel::class.java)
        // TODO: Use the ViewModel
    }

}