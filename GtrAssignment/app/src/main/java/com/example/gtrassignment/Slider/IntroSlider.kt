package com.example.gtrassignment.Slider

data class IntroSlider(
    val image: Int,
    val icon: Int
)