package com.example.gtrassignment.Model

import android.graphics.pdf.PdfDocument.PageInfo
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class GetDataResponse {
    @SerializedName("Success")
    @Expose
    var success: Int? = null

    @SerializedName("error")
    @Expose
    var error: Boolean? = null

    @SerializedName("ProductList")
    @Expose
    var productList: List<Product>? = null

    @SerializedName("PageInfo")
    @Expose
    var pageInfo: PageInfo? = null

    inner class Product {
        @SerializedName("Id")
        @Expose
        var id: Int? = null

        @SerializedName("Type")
        @Expose
        var type: String? = null

        @SerializedName("ParentCode")
        @Expose
        var parentCode: Any? = null

        @SerializedName("Name")
        @Expose
        var name: String? = null

        @SerializedName("Code")
        @Expose
        var code: String? = null

        @SerializedName("Price")
        @Expose
        var price: Double? = null

        @SerializedName("CostPrice")
        @Expose
        var costPrice: Double? = null

        @SerializedName("UnitName")
        @Expose
        var unitName: String? = null

        @SerializedName("CategoryName")
        @Expose
        var categoryName: String? = null

        @SerializedName("BrandName")
        @Expose
        var brandName: String? = null

        @SerializedName("ModelName")
        @Expose
        var modelName: String? = null

        @SerializedName("VariantName")
        @Expose
        var variantName: String? = null

        @SerializedName("SizeName")
        @Expose
        var sizeName: String? = null

        @SerializedName("ColorName")
        @Expose
        var colorName: String? = null

        @SerializedName("OldPrice")
        @Expose
        var oldPrice: Double? = null

        @SerializedName("ImagePath")
        @Expose
        var imagePath: Any? = null

        @SerializedName("ProductBarcode")
        @Expose
        var productBarcode: String? = null

        @SerializedName("Description")
        @Expose
        var description: String? = null

        @SerializedName("ChildList")
        @Expose
        var childList: List<List<Int>>? = null

        @SerializedName("WarehouseList")
        @Expose
        var warehouseList: List<Warehouse>? = null

        @SerializedName("CurrentStock")
        @Expose
        var currentStock: Double? = null

        @SerializedName("CreateDate")
        @Expose
        var createDate: String? = null

        @SerializedName("UpdateDate")
        @Expose
        var updateDate: String? = null

        inner class Warehouse {
            @SerializedName("CostCalculatedId")
            @Expose
            var costCalculatedId: Int? = null

            @SerializedName("WhShortName")
            @Expose
            var whShortName: String? = null

            @SerializedName("CurrentStock")
            @Expose
            var currentStock: Double? = null

            @SerializedName("AverageCosting")
            @Expose
            var averageCosting: Double? = null

            @SerializedName("CostingValue")
            @Expose
            var costingValue: Double? = null

            @SerializedName("SalesValue")
            @Expose
            var salesValue: Double? = null
        }
    }

    inner class PageInfo {
        @SerializedName("PageNo")
        @Expose
        var pageNo: Int? = null

        @SerializedName("PageSize")
        @Expose
        var pageSize: Int? = null

        @SerializedName("PageCount")
        @Expose
        var pageCount: Int? = null

        @SerializedName("TotalRecordCount")
        @Expose
        var totalRecordCount: Int? = null
    }
}