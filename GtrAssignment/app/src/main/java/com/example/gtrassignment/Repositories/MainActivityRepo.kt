package com.example.gtrassignment.Repositories

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.gtrassignment.Common.Common
import com.example.gtrassignment.Model.GetDataResponse
import com.example.gtrassignment.Network.ApiService
import com.example.gtrassignment.Network.RetrofitClient
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivityRepo {
    /*private val apiService: ApiService
    private val compositeDisposable = CompositeDisposable()

    init {
        apiService = Common.getAPIService
    }

    val getDataModelLiveData: MutableLiveData<MutableList<GetDataResponse>>
        get() {
            val data: MutableLiveData<MutableList<GetDataResponse>> =
                MutableLiveData<MutableList<GetDataResponse>>()

            val b_token = "Bearer $auth_token"
            compositeDisposable.add(apiService.getData()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { getModels ->
                    if (getModels != null) {
                        data.value = getModels
                        Log.e("TAG", "Data: " + getModels.size)
                    }
                })

            return data
        }*/

    private val TAG = "DataListRepo"
    private var apiService: ApiService = RetrofitClient.getAPIService

    fun getDataModelLiveData(auth_token: String): LiveData<GetDataResponse> {
        val data = MutableLiveData<GetDataResponse>()
        val b_token = "Bearer $auth_token"
        apiService.getData(b_token).enqueue(object : Callback<GetDataResponse> {
            override fun onResponse(
                call: Call<GetDataResponse>,
                response: Response<GetDataResponse>
            ) {

                if (response.isSuccessful) {
                    data.value = response.body()
                    Log.e(TAG, "onResponse: " + data.value!!.productList!!.size)
                } else {
                    data.value = null
                }
            }

            override fun onFailure(call: Call<GetDataResponse>, t: Throwable) {
                Log.e(TAG, "onFailure: " + t.message)
                data.value = null
            }
        })
        return data
    }

}